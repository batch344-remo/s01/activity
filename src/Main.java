import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        Scanner myObj = new Scanner(System.in);


        System.out.println("What is your name?");
        System.out.println("Enter your first name: ");
        firstName = myObj.nextLine();
        System.out.println("Enter your last name: ");
        lastName = myObj.nextLine();

        System.out.println("What are your grades in Subjects A, B and C?");
        System.out.println("Enter your grade in Subject A: ");
        firstSubject = myObj.nextDouble();
        System.out.println("Enter your grade in Subject B: ");
        secondSubject = myObj.nextDouble();
        System.out.println("Enter your grade in Subject C: ");
        thirdSubject = myObj.nextDouble();

        System.out.println("Hi there " + firstName + " " + lastName + ". Your average for the aforementioned subjects above is: " + ((firstSubject + secondSubject + thirdSubject)/3));
    }
}